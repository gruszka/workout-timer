# -*- coding: utf-8 -*-

from kivy.app import App
from kivy.clock import Clock
from kivy.core.audio import SoundLoader
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty, ListProperty, StringProperty, NumericProperty

class Workout(): # {{{

	def seconds_to_time( self, seconds ):
		return u'%02d:%02d'%divmod( seconds, 60 )

	def _display_update( self ): # {{{
		self.timer.current_interval_value = self.seconds_to_time( self.current_interval_value )
		self.timer.next_interval_value = self.seconds_to_time( self.next_interval_value )
		#TODO zrobic jakos ladniej
		try:
			self.timer.current_interval_progress_bar_value = 1 - ( self.current_interval_value * 100 / self.interval_list[self.current_interval_index] * 0.01 )
		except:
			 self.timer.current_interval_progress_bar_value = 0
 # }}}

	def __init__( self, timer, interval_list ): # {{{
		self.countdown_sound = SoundLoader.load( '/home/grucha/p/kivy/workout_timer/audio/countdown.wav' )
		self.interval_finish_sound = SoundLoader.load( '/home/grucha/p/kivy/workout_timer/audio/interval_finish.wav' )
		self.timer = timer
		self.interval_list = map( lambda x: int(x), interval_list )
		self.current_interval_index, self.current_interval_value = (0, self.interval_list[0]) if self.interval_list else (None,0)
		self.next_interval_value = self.interval_list[1] if len(self.interval_list) > 1 else 0

		self._display_update()
 # }}}

	def _sound_play( self, interval_value ): # {{{
		sound = False
		if self.current_interval_value <= 5 and self.current_interval_value >= 1:
			sound = self.countdown_sound
		elif self.current_interval_value == 0:
			sound = self.interval_finish_sound

		if sound:
			if sound.status == 'play':
				sound.stop()
			sound.play()
 # }}}

	def _update( self, dt ): # {{{
		if self.current_interval_value > 0:
			self.current_interval_value -= 1
			self._sound_play( self.current_interval_value )
		else:
			self.current_interval_index += 1
			self.current_interval_value = self.interval_list[self.current_interval_index] if self.current_interval_index < len(self.interval_list) else 0
			self.next_interval_value = self.interval_list[self.current_interval_index+1] if self.current_interval_index+1 < len(self.interval_list) else 0

		self._display_update()
 # }}}

	def start( self ):
		Clock.schedule_interval(self._update, 1)

	def pause( self ):
		Clock.unschedule(self._update)

	def stop( self ):
		Clock.unschedule(self._update)
		self.interval_list = []
		self.current_interval_value = 0
		self.next_interval_value = 0
		self._display_update()
 # }}}

class Timer( FloatLayout ): # {{{

	workout = None
	counter_time = ObjectProperty()
	interval_list = ListProperty()
	interval_list_text = StringProperty(u'-None-')
	current_interval_value = StringProperty(u'00:00')
	next_interval_value = StringProperty(u'00:00')
	current_interval_progress_bar_value = NumericProperty(0)

	def add_action( self ):
		if self.repetition_number.text and ( self.high_time.text or self.low_time.text ):
			self.interval_list.extend( filter( lambda x: len(x)>0, [self.high_time.text, self.low_time.text] ) * int( self.repetition_number.text ) )
			self.interval_list_text = u', '.join( self.interval_list )

	#TODO clear - wyczyszczenie zaladowanych interwalow
	def load_workout( self ):
		self.workout = Workout( self, self.interval_list )

	def start( self ):
		self.workout.start()

	def pause( self ):
		self.workout.pause()

	def stop( self ):
		self.workout.stop()
 # }}}

class TimerApp( App ):
	def build( self ):
		timer = Timer()
		return timer

if __name__ == '__main__':
	TimerApp().run()
